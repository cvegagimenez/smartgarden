/*
  In this file we will configure all the communications needed to establish the connectivity between
  the embedded system and the MQTT broker.
*/

#include <WiFi.h> // Library to operate with WiFi network
#include <WiFiClient.h> // Library to allow connectivity between the system and the WiFi
#include <PubSubClient.h> // Library to operate with MQTT Broker
#include <stdio.h>

WiFiClient wifi_client;
PubSubClient mqtt_client(mqtt_server, mqtt_port, wifi_client);

/* 
  Callback to receive messages through MQTT. When we use this, it will activate the whaterflow.
  @param topic Topic name where was published the message.
  @param payload Message bytes published in the topic.
  @param lenght Length of the message.
  @return void
*/
void callback(char* topic, byte* payload, unsigned int length) {
  if (String(topic) == remoteWaterActivationTopic){
    Serial.print("Activating water flow.");
    startWater(true);
    delay(10000);
    startWater(false);
  } else {
    Serial.println("Incorrect topic or value");
  }
}

/* 
  Loop calling the MQTT loop to mantain controlled when to execute the operations. It is not used now because we controll it by the publisher.
  @return void
*/
void mqtt_loop(){
  mqtt_client.loop2();
}

/* 
  Function to connect and reconnect to MQTT broker when disconnected.
  @return void
*/
void mqtt_reconnect(){
  if (!mqtt_client.connected()) {
    Serial.println("Disconnected. Reconnecting....");

    if(!mqtt_client.connect(mqtt_id)) {
      Serial.println("MQTT connection failed");
    } else {
      subscribeMqtt();
      Serial.println("Connection success");
    }
  }
}

/* 
  Allows to configure the connection with the broker. It is Managed in the declaration of the class.
  @return void
*/
void mqtt_setup(){
  mqtt_client.setServer(mqtt_server, mqtt_port);
  mqtt_client.setCallback(callback);
}

/* 
  Function that parses the information from the sensors and calls the generic publish function.
  It also concatenates the timestamp in epoch format.
  @return void
*/
void publishMetrics(){
  unsigned long getTime = getTimeNtp();
  if (getTime != 0){
    char hum[16];
    char temp[16];
    char light[16];
    char soil[16];
    char waterflow[16];
    // char humArray[4];
    // char tempArray[4];
    // char lightArray[4];
    // char soilArray[4];
    // char waterflowArray[4];
    // sprintf(hum, "%Lu|%s", getTime, dtostrf(humMetrics(),3, 1, humArray));
    // sprintf(temp, "%Lu|%s", getTime, dtostrf(tempMetrics(),3, 1, tempArray));
    // sprintf(light, "%Lu|%s", getTime, dtostrf(lightMetrics(),3, 1, lightArray));
    // sprintf(soil, "%Lu|%s", getTime, dtostrf(soilMetrics(),3, 1, soilArray));
    // sprintf(waterflow, "%Lu|%s", getTime, dtostrf(waterFlowMetrics(),3, 1, waterflowArray));
    sprintf(hum, "%Lu|%.1f", getTime, humMetrics());
    sprintf(temp, "%Lu|%.1f", getTime, tempMetrics());
    sprintf(light, "%Lu|%.1f", getTime, lightMetrics());
    sprintf(soil, "%Lu|%.1f", getTime, soilMetrics());
    sprintf(waterflow, "%Lu|%.1f", getTime, waterFlowMetrics());
    publishMqtt(humTopic, hum);
    Serial.println(" ");
    Serial.print("Hum: ");
    Serial.println(hum);
    publishMqtt(tempTopic, temp);
    Serial.print("Temp: ");
    Serial.println(temp);
    publishMqtt(lightTopic, light);
    Serial.print("Light: ");
    Serial.println(light);
    publishMqtt(soilHumTopic, soil);
    Serial.print("Soil: ");
    Serial.println(soil);
    publishMqtt(waterFlowTopic, waterflow);
    Serial.print("Water Flow (L/h): ");
    Serial.println(waterflow);
    Serial.println(" ");
  }  
}

/*
  Function to subscribe topics
  @return void
*/
void subscribeMqtt(){
  if(mqtt_client.subscribe(remoteWaterActivationTopic)) {
    Serial.println("Subscription successfull");
  }
}

/*
  Generic function to publish in each topic.
  @param topic Topic to publish.
  @param message Message to public in the topic.
  @return void
*/
void publishMqtt(char topic[], char message[]){
  if(!mqtt_client.publish(topic,message)) {
    Serial.println("Publish failed");
  }
}

/*
  Function to configure the WiFi connection.
  @return void
*/
void establish_wifi_connection(){
  WiFi.begin(wifi_ssid, wifi_password);
  
  while ( WiFi.status() != WL_CONNECTED) {
    Serial.print("Connecting to WiFi");
    delay(300);
  }
  
  Serial.println("\nYou're connected to the network");
  Serial.println("Waiting for an ip address");
  
  while (WiFi.localIP() == INADDR_NONE) {
    Serial.print(".");
    delay(300);
  }
  
  Serial.println("\nIP Address obtained");
  Serial.println(WiFi.localIP());
  
  uint8_t tries = 0;
  while (wifi_client.connect(test_server, test_connection_port) == false) {
    Serial.print(".");
    if (tries++ > 100) {
      Serial.println("\nThe server isn't responding");
      while(1);
    }
    delay(100);
  }
  Serial.println("\nConnected to the server!");
}
