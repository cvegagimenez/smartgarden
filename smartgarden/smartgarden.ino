#if defined(ENERGIA) // LaunchPad MSP430, Stellaris and Tiva, Experimeter Board FR5739 specific
#include "Energia.h"
#else // error
#error Platform not defined
#endif

void setup() {
  Serial.begin(115200);
  establish_wifi_connection();
  beginDHT();
  beginWaterFlowSensor();
  beginUdp();
}

void loop() {
  mqtt_reconnect();
  mqtt_loop();
  publishMetrics();
  delay(60000);
}
