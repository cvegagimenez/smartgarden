/*
File where define the behavour of the sensors.
*/

#include <DHT.h>
DHT dhtSensor(DHTPIN, DHTTYPE);

float lightness = 0;
float soilHumidity = 0;
float h;
float t;
boolean dhtFlag;
int pulse;
int pulseFrequency;

/* 
  Function to start the DHT sensor.
  @return void
*/
void beginDHT(){
  dhtSensor.begin();
}

/*
  Function to get humidity metrics from DHT sensor.
  @return Humidity value as float.
*/
float humMetrics(){
  h = dhtSensor.readHumidity();

  return h;
}

/*
  Function to get temperature metrics from DHT sensor.
  @return Temperature value as float.
*/
float tempMetrics(){
  t = dhtSensor.readTemperature(false);

  return t;
}

/*
  Function to get lightness from sensor.
  @return Lightness value as float
*/
float lightMetrics(){
  lightness = map(analogRead(lightnessPin), MIN_VALUE, MAX_VALUE, MIN_PCT, MAX_PCT);
  
  return lightness;
}

/*
  Function to get soil humidity from sensor.
  @return Soil humidity value as float.
*/
float soilMetrics(){
  soilHumidity = map(analogRead(soilHumidityPin), MIN_VALUE, MAX_VALUE, MIN_PCT, MAX_PCT);

  if (soilHumidity > 80){
    startWater(true);
  } else {
    startWater(false);
  } 
  
  return soilHumidity;
}

/*
  Function to get waterflow metrics.
  @return Water flow metric as float.
*/
float waterFlowMetrics(){
  float freq = calculateWaterFlow();
  float flow = freq*60/waterFlowFactor; // We get flow in L/h
  return flow;
}

/* 
  Function to setup YF-201 and configure the interrution for the sensor pulse.
  @return void
*/
void beginWaterFlowSensor(){
  pinMode(waterFlowPin, INPUT_PULLUP);
  attachInterrupt(waterFlowPin,getPulses,RISING);
}

/*
  Function to increase number of pulses from the waterflow sensor on each interruption.
  @return void
*/
void getPulses(){
  pulse++;
}

/*
  Function to get water flow frequency.
  @return Frequency of pulses for the waterflow sensor.
*/
int calculateWaterFlow(){
  noInterrupts();
  delay(1000);
  pulseFrequency = pulse;
  pulse = 0;
  interrupts();
  return pulseFrequency;
}