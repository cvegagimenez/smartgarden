/*
File where we will configure the actuator that will be activated when something triggers it.
*/

boolean waterState = false;

/*
  Function to start/stop the water flow.
  @param waterState State that we want to set to the water.
  @return void
*/
void startWater(bool waterState){
    if (waterState) {
    digitalWrite(solenoidPin, HIGH);
    digitalWrite(RED_LED, HIGH);
    } else {
    digitalWrite(solenoidPin, LOW);
    digitalWrite(RED_LED, LOW);
    }
}

/*
  Function to setup the electrovalve.
  @return void
*/
void configureSolenoid(){
    pinMode(solenoidPin, OUTPUT);
    pinMode(RED_LED, OUTPUT);
    Serial.println("Configured solenoid.");
}