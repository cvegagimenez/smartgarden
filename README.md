# Smartgarden

This project wants to offer a DIY alternative to similar devices for automating the irrigation process in gardens that are in the market right now. Those devices use to be expensive and can not be escalated nor allow the monitorization of the garden. The proposed project allows visualization of real time measured data by the sensors installed and the activation of water system. This gives us a historic to determine when the automation should be activated or to activate the water flow by a manual process. 

The metrics captured by the sensor would be published to internet through the MQTT protocol from the embeded device and a backend service will take those metrics and will post them in a database in order to be consumed by the frontend service. The publication on the internet allows the system to escalate adding other gardens to the platform just by filtering in the frontend service.


Sources

DHT22
    - https://platformio.org/lib/show/58/DHT22

LCD 
    - https://usermanual.wiki/Pdf/LCDscreen2020Reference20Manual.1819409514/html#pf21

KY70
    - https://randomnerdtutorials.com/guide-for-soil-moisture-sensor-yl-69-or-hl-69-with-the-arduino/

YF-201 
    - https://www.electroschematics.com/working-with-water-flow-sensors-arduino/
    - https://energia.nu/reference/en/language/functions/external-interrupts/attachinterrupt/
    - https://e2e.ti.com/support/microcontrollers/msp430/f/166/t/506515?Energia-MSP432-digitalRead-not-working-in-Interrupt-

Date/Time - https://github.com/rei-vilo/DateTime_Library

InfluxDB + Grafana + K8s 
    - https://opensource.com/article/19/2/deploy-influxdb-grafana-kubernetes
    - INFLUX HELM https://github.com/helm/charts/tree/master/stable/influxdb
    - GRAFANA HELM https://github.com/helm/charts/tree/master/stable/grafana

MQTT 
    - https://alvaromonsalve.com/2018/04/02/cliente-mosquitto-paho-mqtt/
    - https://techtutorialsx.com/2017/04/24/esp32-subscribing-to-mqtt-topic/
    - https://randomnerdtutorials.com/esp32-mqtt-publish-subscribe-arduino-ide/

Python InfluxDB 
    - http://www.compassmentis.com/2016/06/grafana-influxdb-and-python-simple-sample/

SNTP
    - http://www.geekstips.com/arduino-time-sync-ntp-server-esp8266-udp/
    - https://lastminuteengineers.com/esp32-ntp-server-date-time-tutorial/
