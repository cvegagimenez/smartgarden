# GLOBAL
APP_NAME = 'smartgarden'

# MQTT
MQTT_SERVER = '80.211.155.16'
MQTT_PORT = 1883

MQTT_HUM_TOPIC = '/smartgarden/garden1/env_humidity'
MQTT_TEMP_TOPIC = '/smartgarden/garden1/env_temperature'
MQTT_LIGHT_TOPIC = '/smartgarden/garden1/lightness'
MQTT_SOIL_HUM_TOPIC = '/smartgarden/garden1/soil_humidity'
MQTT_TOPIC_PREFIX = '/smartgarden/garden1/+'
MQTT_REMOTE_WATER_TOPIC = '/smartgarden/garden1/start_water'

START_WATER_MESSAGE = 'start_water'

#INFLUX
INFLUXDB_ADDRESS = '80.211.155.16'
INFLUXDB_PORT = 8086
INFLUXDB_USER = 'root'
INFLUXDB_PASSWORD = 'root'
INFLUXDB_DATABASE = 'smartgarden'
INFLUXBD_RETENTION_TIME = '356d'
INFLUXBD_RETENTION_POLICY_NAME = 'old_metrics'